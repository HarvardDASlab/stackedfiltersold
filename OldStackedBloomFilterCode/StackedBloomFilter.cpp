#include "StackedBloomFilter.h"

StackedBloomFilter::StackedBloomFilter() {}

StackedBloomFilter::StackedBloomFilter(uint32 num_layers,
                                       std::vector<Element> positives,
                                       std::vector<Element> negatives,
                                       size_t total_size, double psi,
                                       double penalty_coef,
                                       bool equal_layer_fprs,
                                       std::vector<double> layer_fprs) {
  penalty_coef_ = penalty_coef;
  equal_layer_fprs_ = equal_layer_fprs;
  num_layers_ = num_layers;
  num_positive_ = positives.size();
  num_negative_ = negatives.size();
  printf("Num_Negative:%d Num_Positive:%d\n", num_negative_, num_positive_);
  total_size_ = total_size;
  psi_ = psi;
  beta_ = (double)num_positive_ / (double)(num_positive_ + num_negative_);
  if (layer_fprs.size() == 0) layer_fprs = EstimateLayerFPR();
  filter_array_ = std::vector<BloomFilter>();
  // Preallocate each of the bloom filters to their estimated sizes
  double positive_fpr = 1;
  double negative_fpr = 1;
  for (int i = 0; i < num_layers; i++) {
    int num_hashes = std::max((int)(round(-log(layer_fprs[i]) / log(2))), 1);
    size_t size = 0;
    if ((i % 2) == 0) {
      size = 1 /
             (1 - (double)pow(
                      (1 - (double)pow(layer_fprs[i], (double)1 / num_hashes)),
                      (double)1 / (positive_fpr * num_positive_ * num_hashes)));
      negative_fpr *= layer_fprs[i];
    } else {
      size = 1 /
             (1 - (double)pow(
                      (1 - (double)pow(layer_fprs[i], (double)1 / num_hashes)),
                      (double)1 / (negative_fpr * num_negative_ * num_hashes)));
      positive_fpr *= layer_fprs[i];
    }
    // Bloom Filter FPR formulas do not work well on small filters, so we put a
    // floor on the size of each layer.
    if (size < 2000 && layer_fprs[i] < .99) size = 2000;
    filter_array_.push_back(BloomFilter(size, num_hashes, rand()));
  }
  for (int i = 0; i < num_positive_; i++) {
    filter_array_[0].addElement(positives[i]);
  }
  // Add the elements to the filters in a descending order by layer
  // (filter-pair)
  size_t num_negative_fp = num_negative_;
  size_t num_positive_fp = num_positive_;
  std::vector<Element> negative_fp(negatives.begin(), negatives.end());
  std::vector<Element> positive_fp(positives.begin(), positives.end());
  for (int i = 0; i < num_layers_ - 1; i++) {
    if ((i % 2) == 0) {
      int temp_neg_fp = 0;
      for (int j = 0; j < num_negative_fp; j++) {
        if (filter_array_[i].testElement(negative_fp[j])) {
          negative_fp[temp_neg_fp] = negative_fp[j];
          temp_neg_fp++;
        }
      }
      num_negative_fp = temp_neg_fp;
      for (int j = 0; j < num_negative_fp; j++) {
        filter_array_[i + 1].addElement(negative_fp[j]);
      }
    } else {
      int temp_pos_fp = 0;
      for (int j = 0; j < num_positive_fp; j++) {
        if (filter_array_[i].testElement(positive_fp[j])) {
          positive_fp[temp_pos_fp] = positive_fp[j];
          temp_pos_fp++;
        }
      }
      num_positive_fp = temp_pos_fp;
      for (int j = 0; j < num_positive_fp; j++) {
        filter_array_[i + 1].addElement(positive_fp[j]);
      }
    }
  }
}

std::vector<double> StackedBloomFilter::EstimateLayerFPR() {
  double one_level_fpr = exp(-(long long)total_size_ * log(2) * log(2) / beta_ /
                             (num_negative_ + num_positive_));
  std::vector<double> one_layer_fprs =
      std::vector<double>(num_layers_, one_level_fpr);
  for (int i = 1; i < num_layers_; i++) one_layer_fprs[i] = 1;
  if (num_layers_ == 1) {
    filter_fprs_ = one_layer_fprs;
    return filter_fprs_;
  }
  double* zeros = (double*)calloc(num_layers_, sizeof(double));
  for (int i = 0; i < num_layers_; i++) zeros[i] = 0.00000000000000000001;
  double* ones = (double*)calloc((num_layers_), sizeof(double));
  for (int i = 0; i < num_layers_; i++) ones[i] = 1;
  nlopt_opt equal_fpr_opt = nlopt_create(NLOPT_GN_ISRES, 1);
  nlopt_set_lower_bounds(equal_fpr_opt, zeros);
  nlopt_set_upper_bounds(equal_fpr_opt, ones);
  nlopt_set_maxtime(equal_fpr_opt, .25);
  nlopt_add_inequality_constraint(equal_fpr_opt,
                                  &StackedBloomFilter::SizeFunctionEqual, this,
                                  total_size_ * .0005);
  nlopt_set_min_objective(equal_fpr_opt, &StackedBloomFilter::FprFunctionEqual,
                          this);
  double equal_fpr_fpr = 0;
  std::vector<double> lfprs(1, .5);
  if (nlopt_optimize(equal_fpr_opt, lfprs.data(), &equal_fpr_fpr) < 0)
    printf("Equal Opt Error!!\n");
  std::vector<double> equal_fprs = std::vector<double>(num_layers_, lfprs[0]);
  if (equal_layer_fprs_) {
    filter_fprs_ = equal_fprs;
    return filter_fprs_;
  }
  if (equal_fpr_fpr > one_level_fpr) {
    filter_fprs_ = one_layer_fprs;
    printf("Using One Level Start\n");
  } else {
    printf("Using Equal Level Start\n");
    filter_fprs_ = equal_fprs;
  }
  nlopt_opt local_fpr_opt = nlopt_create(NLOPT_LN_COBYLA, num_layers_);
  nlopt_set_lower_bounds(local_fpr_opt, zeros);
  nlopt_set_upper_bounds(local_fpr_opt, ones);
  nlopt_set_maxtime(
      local_fpr_opt,
      .5 + 1 * (num_layers_ - 1));  // Providing more time for more parameters
  nlopt_set_ftol_rel(local_fpr_opt, .000001);
  nlopt_add_inequality_constraint(local_fpr_opt,
                                  &StackedBloomFilter::SizeFunctionVaried, this,
                                  total_size_ * .0005);
  nlopt_set_min_objective(local_fpr_opt, &StackedBloomFilter::FprFunctionVaried,
                          this);
  double variable_fpr_fpr = 1;
  nlopt_result local_ret_status =
      nlopt_optimize(local_fpr_opt, filter_fprs_.data(), &variable_fpr_fpr);
  if (local_ret_status == -4)
    printf("ERROR!!!!!  Roundoff Errors Reached in Local Optimization\n");
  else if (local_ret_status < 0)
    printf("ERROR!!! GENERALLY SPEAKING IN LOCAL\n");
  return filter_fprs_;
}

bool StackedBloomFilter::TestElement(Element element) {
  for (int i = 0; i < num_layers_; i++) {
    if ((i % 2) == 0) {
      if (filter_array_[i].testElement(element) == false) return false;
    } else {
      if (filter_array_[i].testElement(element) == false) return true;
    }
  }
  return true;
}

void StackedBloomFilter::AddPositiveElement(Element element) {
  filter_array_[0].addElement(element);
  for (int i = 0; i < (num_layers_ - 1) / 2; i++) {
    if (filter_array_[2 * i + 1].testElement(element) == true) {
      filter_array_[2 * i + 2].addElement(element);
    } else {
      return;
    }
  }
}

size_t StackedBloomFilter::TotalSize() {
  size_t size = 0;
  for (int i = 0; i < num_layers_; i++) {
    size += filter_array_[i].GetSize();
  }
  return size;
}

size_t StackedBloomFilter::NumFilterChecks() {
  size_t num_filter_checks = 0;
  for (int i = 0; i < num_layers_; i++) {
    num_filter_checks += filter_array_[i].num_checks_;
  }
  return num_filter_checks;
}

void StackedBloomFilter::ResetNumFilterChecks() {
  for (int i = 0; i < num_layers_; i++) filter_array_[i].num_checks_ = 0;
}

void StackedBloomFilter::PrintLayerDiagnostics() {
  for (int i = 0; i < num_layers_; i++) {
    printf(
        "Layer %d FPR: %.20f Size:%d Num Hashes:%d Num Elements:%ld Load "
        "Factor:%f \n",
        i, filter_fprs_[i], filter_array_[i].GetSize(),
        filter_array_[i].num_hashes_, filter_array_[i].GetNumElements(),
        filter_array_[i].GetLoadFactor());
  }
}
