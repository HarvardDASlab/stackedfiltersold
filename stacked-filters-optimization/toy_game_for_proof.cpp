// hello.cpp
#include <iostream>
#include <vector>
#include <string>
#include <cstdarg>
#include <cmath>
#include <cstring>
#include <algorithm>    // std::min
using namespace std;
#include <queue>          // std::queue
#include <map>

//int c = 2;

map<pair<int,int>, long long int> solvedPairs;


long long int playGameDepthFirst(double c, double g_prime) {
  long long int countBelow = 1;
  if (c <= 0) {
  	return countBelow;
  }
  for (int f1 = 1; f1 <= int(min(20.0, ceil(c))); f1++) {
  	int cap1 = ceil(pow(2, g_prime + f1) * (1.0 / log(2)));
  	//int cap1 = pow(2, g_prime + f1) * (1.0 / log(2));
  	if (g_prime + f1 == 0) {
  		cout << cap1 << endl;
  	}
  	int cap2 = 16;
  	int cap = min(cap1, cap2);
  	for (int f2 = 1; f2 < cap; f2++) {
  		long long int countBelowToAdd = 0;
  		if (solvedPairs.find(make_pair(c-f1, g_prime + f1 - f2)) == solvedPairs.end()) {
  			countBelowToAdd = playGameDepthFirst(c - f1, g_prime + f1 - f2);
  		} else {
  			countBelowToAdd = solvedPairs[make_pair(c-f1, g_prime + f1 - f2)];
  		}
  		//if (c - f1 != 0) {
  		//	cout << "c: " << c - f1 << ", g': " << g_prime + f1 - f2 << " count:" << countBelowToAdd << endl;
  		//}
  		countBelow += countBelowToAdd;
  	}
  }
  solvedPairs.insert( std::pair<std::pair<int,int>,long long int>(make_pair(c, g_prime),countBelow) );

  return countBelow;
}

int main(int argc, char *argv[]) {
  // argument 1: minimization of fpr or space
  // argument 2: constraint (either FPR or space in bits per element)
  // argument 3: psi
  // argument 4: N / P
  // Example: ./filter_optimizer fpr 10 0.8 2
  // translation: minimize fpr for a stacked filter using less than 10 bits per element, given psi=0.8, N/P = 2
  if (argc < 2) {
    cout << "There should be the following 4 arguments: the minimization (fpr or space),"
    "the constraint (the other of fpr or space), psi, N/P, multiplicative slack " << endl;
    cout << "Ex: ./filter_optimizer fpr 10 0.8 2 0.001" << endl;
    return 0;
  }
  double goal = std::stod(argv[1]);
  double initialPoverN = std::stod(argv[2]);
  cout << printf("%f, %f", goal, initialPoverN) << endl;

  long long int count = playGameDepthFirst(goal, initialPoverN);
  cout << "initial c: " << goal << ", g: " << initialPoverN << ", count: " << count << endl;

  long long int maxCplusGequalA[45] = {};
  double maxGrowth = 1.0;
  for (auto const& x : solvedPairs) {
  	int c = x.first.first;
  	int g = x.first.second;
    if (x.second > maxCplusGequalA[c+g+10]) {
      maxCplusGequalA[c+g+10] = x.second;
    }
  	
    cout << "c: " << c << ", g:" << g << ", count: " << x.second << endl;
  	
    /*if (solvedPairs.find(make_pair(c+1, g)) != solvedPairs.end()) {
  		double countCPlus = (double) solvedPairs[make_pair(c+1, g)];
  		if ((countCPlus / x.second) > maxGrowth) {
  			maxGrowth = (countCPlus / x.second);
  		}
  		if ((countCPlus / x.second) > 4.0) {
  			cout << "c: " << c << ", " << "g: " << g << ", count: " << x.second << endl;
  			cout << "c+1: " << c+1 << ", " << "g: " << g << ", count: " << countCPlus << endl;
  		}
  	} */
  	if (solvedPairs.find(make_pair(c, g+1)) != solvedPairs.end()) {
  		double countCPlus = (double) solvedPairs[make_pair(c, g+1)];
  		if ((countCPlus / x.second) > maxGrowth) {
  			maxGrowth = (countCPlus / x.second);
        cout << "C: " << c << ", G: " << g << ", G+1:" << g+1 << ", growth: " << maxGrowth << endl;
  		}
  		if ((countCPlus / x.second) > 4.0) {
  			cout << "c: " << c << ", " << "g: " << g << ", count: " << x.second << endl;
  			cout << "c: " << c << ", " << "g+1: " << g+1 << ", count: " << countCPlus << endl;
        cout << "CHECK THIS LINE YOU BIG DUMMIE" << endl;
  		}
  	}
  }
  double maxGrowthA = 1.0;
  for (int i = 1; i < 45; i++) {
    if (maxCplusGequalA[i] != 0) {
      cout << "c + g = " << i-10 << ": " << maxCplusGequalA[i] << endl;
      cout << "growth: " << double(maxCplusGequalA[i]) / maxCplusGequalA[i-1] << endl;
      if (double(maxCplusGequalA[i]) / maxCplusGequalA[i-1] > maxGrowthA) {
        maxGrowthA = double(maxCplusGequalA[i]) / maxCplusGequalA[i-1];
      }
    }
  }

  cout << 1 / log(2) << endl;
  cout << maxGrowthA << endl;
  cout << maxGrowth << endl;


  return 0;
}