#include <iostream>
#include <vector>

using namespace std;

#include <map>

class ZipfianDistribution {
public:
    double zipfianParam;
    int numElements;
    double totalHarmonicVal;
    std::vector<double> psiVals;

    ZipfianDistribution(double zipfianParam, int numElements);
}; 