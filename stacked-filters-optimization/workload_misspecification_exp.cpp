#include <iostream>
#include <vector>
#include <string>
#include <cstdarg>
#include <cmath>
#include <cstring>
#include <algorithm>    // std::min
using namespace std;
#include <queue>          // std::queue
#include <chrono> 
using namespace std::chrono;
#include "nlopt.h"

#include "workload_generator.h"

int ONE_MILLION = 1000000;
int ONE_THOUSAND = 1000;
int TEN_THOUSAND = 10000;
int ONE_HUNDRED_THOUSAND = 100000;

//WRITE COUNTING OF EACH DEPTH.

class OptimizationObjectNLOpt {
  public:
    double constraint;
    double psi;
    double NoverP;
    double slack;
    std::vector<double> bitsPerElementLayers;
    OptimizationObjectNLOpt();
    OptimizationObjectNLOpt(double constraint, double psi, double NoverP);
};

OptimizationObjectNLOpt::OptimizationObjectNLOpt(double constraint, double psi, double NoverP) {
  this->constraint = constraint;
  this->psi = psi;
  this->NoverP = NoverP;
}

double FprFunctionEqual(unsigned num_layers, const double *layer_fprs,
 double *grad, void *filter_ptr) {
    OptimizationObjectNLOpt *filter =
    (OptimizationObjectNLOpt *) filter_ptr;
    double psi = filter->psi;
    const double layer_fpr = layer_fprs[0];
    double known_fpr = layer_fpr;
    for (unsigned int i = 1; i <= (num_layers - 1) / 2; i++) {
        known_fpr = known_fpr * layer_fpr;
    }
    double unknown_fpr_side = 0;
    for (unsigned int i = 1; i <= (num_layers - 1) / 2; i++) {
        double temp_fpr = layer_fpr;
        for (unsigned int j = 1; j <= 2 * (i - 1); j++) {
            temp_fpr = temp_fpr * layer_fpr;
        }
        unknown_fpr_side += temp_fpr * (1 - layer_fpr);
    }
    double unknown_fpr_end = layer_fpr;
    for (unsigned int i = 1; i < num_layers; i++) {
        unknown_fpr_end = unknown_fpr_end * layer_fpr;
    }
    return (psi * known_fpr + (1 - psi) * (unknown_fpr_side + unknown_fpr_end));
}

double fprForBitsPerEleBF(double bitsPerEle) {
  return min(1.0, pow(2, -1 * log(2) * bitsPerEle));
}

double sizeForFprBF(double inputFPR) {
  return -log(inputFPR) * (1.0 / pow(log(2), 2));
}

double SizeFunctionEqual(unsigned num_layers, const double *layer_fprs,
    double *grad, void *filter_ptr) {
    OptimizationObjectNLOpt *filter = (OptimizationObjectNLOpt *) filter_ptr;
    double total_size = filter->constraint * .995;
    double NoverP = filter->NoverP;
    double size = 0;
    double positive_fpr = 1;
    double negative_fpr = 1;
    const double layer_fpr = layer_fprs[0];
    for (unsigned int i = 0; i < num_layers; i++) {
        if ((i % 2) == 0) {
            size += -log(layer_fpr) * (1.0 / pow(log(2), 2)) * positive_fpr;
            negative_fpr *= layer_fpr;
        } else {
            size += -log(layer_fpr) * (1.0 / pow(log(2), 2)) * negative_fpr * NoverP;
            positive_fpr *= layer_fpr;
        }
    }
    return size - total_size;
}

double equal_fpr_gradientForSize(double NoverP, double alpha) {
  double numeratorPart1 = (alpha - 1) * ((NoverP * alpha) + 1);
  double numeratorPart2 = -1 * (NoverP + 1) * alpha * log(alpha);
  double denominator = pow(1 - alpha, 2) * alpha;

  double unscaledGradient = (numeratorPart1 + numeratorPart2) / denominator;
  double scaledGradient = (1 / pow(log(2), 2)) * unscaledGradient;
  return scaledGradient;
}

double sizeForFixedAlpha(double NoverP, double alpha) {
  return (-1 / pow(log(2), 2)) * log(alpha) * ((1 + NoverP * alpha) / (1 - alpha));
}

// derivation: at infinity layers, known negatives are all caught. For unknown negatives is (1 - alpha) * sum_i=0^inf alpha^(2i+1)
// this is then (1 - alpha) * alpha * sum_i=0^inf (x^2)^i -> traditional power series. 
// these two equations are the same. 
double fprFixedAlpha(double psi, double alpha) {
  return (1 - psi) * (alpha / (alpha + 1));
  //return (1 - psi) * (1 -alpha) * (alpha / 1 - pow(alpha, 2));
}

double optimizeFPRunderSizeConstraintLayerEqual(double constraint, double psi, double NoverP, double initialAlpha, double epsilonGradient) {
  double stepSize = 1;
  double alpha = initialAlpha;
  double size = sizeForFixedAlpha(NoverP, alpha);
  int count = 0;
  while (stepSize > epsilonGradient) {
    double gradient = equal_fpr_gradientForSize(NoverP, alpha);
    if (size > constraint) {
      double newAlpha = alpha - (stepSize * gradient);
      double newSize = sizeForFixedAlpha(NoverP, newAlpha);
      while (newSize > size or newAlpha < 0 or newAlpha > 1) {
        stepSize = stepSize * 0.5;
        newAlpha = alpha - (stepSize * gradient);
        if (stepSize < epsilonGradient) {
          break;
        }
        newSize = sizeForFixedAlpha(NoverP, newAlpha);
      }
      alpha = newAlpha;
      size = sizeForFixedAlpha(NoverP, newAlpha);
    } else {
      double newAlpha = alpha - stepSize;
      while (sizeForFixedAlpha(NoverP, newAlpha) > constraint or newAlpha < 0) {
        stepSize = stepSize * 0.5;
        newAlpha = alpha - stepSize;
        if (stepSize < epsilonGradient) {
          break;
        }
      }
      size = sizeForFixedAlpha(NoverP, newAlpha);
      alpha = newAlpha;
    }
    count += 1;
  }
  if (sizeForFixedAlpha(NoverP, alpha) > constraint + 0.01) {
    alpha = -1.0;
  }
  return alpha;
}

int main(int argc, char *argv[]) {
  // argument 1: minimization of fpr or space
  // argument 2: constraint (either FPR or space in bits per element)
  // argument 3: psi
  // argument 4: N / P
  // Example: ./filter_optimizer fpr 10 0.8 2
  // translation: minimize fpr for a stacked filter using less than 10 bits per element, given psi=0.8, N/P = 2

  if (argc < 8) {
    cout << "There should be the following 6 arguments: the minimization (fpr or space),"
    "the constraint (the other of fpr or space), multiplicative slack, positive elements (in millions), negative elements (in millions), max # negs to take, zipf" << endl;
    cout << "Ex: ./full_workflow_filter_optimizer fpr 10.1 0.001 1.2 10 2.0 1.0" << endl;
    return 0;
  }

  std::string minimization = argv[1];
  double constraint = std::stod(argv[2]);
  double multiplicativeSlack = std::stod(argv[3]);
  double epsilonSlack = std::stod(argv[3]);
  int numPositiveElements = static_cast<int>(std::stod(argv[4]) * ONE_MILLION);
  int numNegativeElements = static_cast<int>(std::stod(argv[5]) * ONE_MILLION);
  int maxNumNegativeElements = static_cast<int>(std::stod(argv[6]) * ONE_MILLION);
  assert(numNegativeElements > (numPositiveElements / 1000));
  double zipfActual = std::stod(argv[7]);
  cout << "zipf is: " << zipfActual;

  ZipfianDistribution distActual = ZipfianDistribution(zipfActual, numNegativeElements);
  double bestFPR = fprForBitsPerEleBF(constraint);
  double originalFPR = bestFPR;
  cout << "base FPR is " << originalFPR << endl;
  //double epsilonSlack = bestFPR * multiplicativeSlack;
  cout << "slack is: " << epsilonSlack << endl;

  vector<double> FPRachieved;

  for (double zipfBelieved = 0.15; zipfBelieved <= 1.36; zipfBelieved += 0.05) {
    cout << "zipf: " << zipfBelieved << ", ";
  // 1) calculate single layer FPR. Use this to get epsilon slack for best FPR allowed. 
    double lastPsi = 0;
    double NoverPused = 0.0;
    ZipfianDistribution distBelieved = ZipfianDistribution(zipfBelieved, numNegativeElements);

    vector<double> bestSetup;

    double bestFPREqual = 1.1;
    double bestAlpha = 1.0;

    lastPsi = 0;
    double NoverPusedEqualFPR;
    double iUsed = 0;

    cout << "constraint is: " << constraint;
    for (int i = 0; i <= maxNumNegativeElements; i+= 5) {
    	double newPsi = distBelieved.psiVals[i-1];
    	double slackFromN = (newPsi - lastPsi) * min(originalFPR / (1 - newPsi), 1.0);
    	if (slackFromN > epsilonSlack / 2) {
    		double NoverP = static_cast<double>(i) / static_cast<double>(numPositiveElements);
    		double alphaBest = optimizeFPRunderSizeConstraintLayerEqual(constraint, newPsi, NoverP, 0.01, 0.0001);
    		lastPsi = newPsi;
    		if (alphaBest > 0 and fprFixedAlpha(newPsi, alphaBest) < bestFPREqual) {
          bestFPREqual = fprFixedAlpha(newPsi, alphaBest);
          NoverPusedEqualFPR = NoverP;
          iUsed = i;
          bestAlpha = alphaBest;
        }
    	}
    }
    double actualPsi = distActual.psiVals[iUsed-1];
    double achievedFPR = fprFixedAlpha(actualPsi, bestAlpha);
    FPRachieved.push_back(achievedFPR);
    cout << ", bestAlpha: " << bestAlpha << ", NoverPused: " << NoverPusedEqualFPR << ", FPR achieved: " << achievedFPR << endl;
  }

  for (auto i: FPRachieved)
    std::cout << i << ", ";
  cout << endl;

  return 0;
}