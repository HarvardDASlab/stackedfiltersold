#include <iostream>
#include <vector>
#include <string>
#include <cstdarg>
#include <cmath>
#include <cstring>
#include <algorithm>    // std::min
using namespace std;
#include <queue>          // std::queue
#include <chrono> 
using namespace std::chrono;
#include "nlopt.h"


#include "workload_generator.h"

int TEN_MILLION = 10000000;
int ONE_MILLION = 1000000;
int ONE_THOUSAND = 1000;
int TEN_THOUSAND = 10000;
int ONE_HUNDRED_THOUSAND = 100000;


class OptimizationObjectNLOpt {
  public:
    double constraint;
    double psi;
    double NoverP;
    double slack;
    std::vector<double> bitsPerElementLayers;
    OptimizationObjectNLOpt();
    OptimizationObjectNLOpt(double constraint, double psi, double NoverP);
};

OptimizationObjectNLOpt::OptimizationObjectNLOpt(double constraint, double psi, double NoverP) {
  this->constraint = constraint;
  this->psi = psi;
  this->NoverP = NoverP;
}

double FprFunctionVaried(unsigned num_layers, const double *layer_fprs,
    double *grad, void *filter_ptr) {
    OptimizationObjectNLOpt *filter =
    (OptimizationObjectNLOpt *) filter_ptr;
    double psi = filter->psi;
    double known_fpr = layer_fprs[0];
    for (unsigned int i = 1; i <= (num_layers - 1) / 2; i++) {
        known_fpr = known_fpr * layer_fprs[2 * i];
    }
    double unknown_fpr_side = 0;
    for (unsigned int i = 1; i <= (num_layers - 1) / 2; i++) {
        double temp_fpr = layer_fprs[0];
        for (unsigned int j = 1; j <= 2 * (i - 1); j++) {
            temp_fpr = temp_fpr * layer_fprs[j];
        }
        unknown_fpr_side += temp_fpr * (1 - layer_fprs[2 * i - 1]);
    }
    double unknown_fpr_end = layer_fprs[0];
    for (unsigned int i = 1; i <= (num_layers - 1) / 2; i++) {
        unknown_fpr_end = unknown_fpr_end * layer_fprs[i];
    }
        // Penalty Function For Number of Hashes
    return (psi * known_fpr + (1 - psi) * (unknown_fpr_side + unknown_fpr_end));
};

double FprFunctionEqual(unsigned num_layers, const double *layer_fprs,
 double *grad, void *filter_ptr) {
    OptimizationObjectNLOpt *filter =
    (OptimizationObjectNLOpt *) filter_ptr;
    double psi = filter->psi;
    const double layer_fpr = layer_fprs[0];
    double known_fpr = layer_fpr;
    for (unsigned int i = 1; i <= (num_layers - 1) / 2; i++) {
        known_fpr = known_fpr * layer_fpr;
    }
    double unknown_fpr_side = 0;
    for (unsigned int i = 1; i <= (num_layers - 1) / 2; i++) {
        double temp_fpr = layer_fpr;
        for (unsigned int j = 1; j <= 2 * (i - 1); j++) {
            temp_fpr = temp_fpr * layer_fpr;
        }
        unknown_fpr_side += temp_fpr * (1 - layer_fpr);
    }
    double unknown_fpr_end = layer_fpr;
    for (unsigned int i = 1; i < num_layers; i++) {
        unknown_fpr_end = unknown_fpr_end * layer_fpr;
    }
    return (psi * known_fpr + (1 - psi) * (unknown_fpr_side + unknown_fpr_end));
}

double SizeFunctionVaried(unsigned num_layers, const double *layer_fprs,
   double *grad, void *filter_ptr) {
    OptimizationObjectNLOpt *filter = (OptimizationObjectNLOpt *) filter_ptr;
    double total_size = filter->constraint * .995;
    double NoverP = filter->NoverP;
    double size = 0;
    double positive_fpr = 1;
    double negative_fpr = 1;
    for (unsigned int i = 0; i < num_layers; i++) {
        double temp_size;
        if (((i + 2) % 2) == 0) {
            temp_size = -log(layer_fprs[i]) * (1.0 / pow(log(2), 2)) * positive_fpr;
            negative_fpr *= layer_fprs[i];
        } else {
            temp_size = -log(layer_fprs[i]) * (1.0 / pow(log(2), 2)) * negative_fpr * NoverP;
            positive_fpr *= layer_fprs[i];
        }
        size += temp_size;
    }
    return size - total_size;
}

double fprForBitsPerEleBF(double bitsPerEle) {
  return min(1.0, pow(2, -1 * log(2) * bitsPerEle));
}

double sizeForFprBF(double inputFPR) {
  return -log(inputFPR) * (1.0 / pow(log(2), 2));
}

double SizeFunctionEqual(unsigned num_layers, const double *layer_fprs,
    double *grad, void *filter_ptr) {
    OptimizationObjectNLOpt *filter = (OptimizationObjectNLOpt *) filter_ptr;
    double total_size = filter->constraint * .995;
    double NoverP = filter->NoverP;
    double size = 0;
    double positive_fpr = 1;
    double negative_fpr = 1;
    const double layer_fpr = layer_fprs[0];
    for (unsigned int i = 0; i < num_layers; i++) {
        if ((i % 2) == 0) {
            size += -log(layer_fpr) * (1.0 / pow(log(2), 2)) * positive_fpr;
            negative_fpr *= layer_fpr;
        } else {
            size += -log(layer_fpr) * (1.0 / pow(log(2), 2)) * negative_fpr * NoverP;
            positive_fpr *= layer_fpr;
        }
    }
    return size - total_size;
}

double calculateSize(std::vector<double> bitsPerElementLayers, double NoverP, bool print) {
  double posSize = 0.0;
  double negSize = 0.0;
  for (unsigned int i = 0; i < bitsPerElementLayers.size(); i++) {
    if (i % 2 == 0) {
      double proportion = 1.0;
      for (unsigned int j = 0; j < i/2; j++) {
        proportion = proportion * fprForBitsPerEleBF(bitsPerElementLayers[(2 * j) + 1]);
      }
      posSize += proportion * bitsPerElementLayers[i];
    } else {
      double proportion = 1.0;
      for (unsigned int j = 0; j < (i+1)/2; j++) {
        proportion = proportion * fprForBitsPerEleBF(bitsPerElementLayers[2 * j]);
      }
      if (print) {
        cout << "proportion is: " << proportion << ", i is: " << i;
      }
      negSize += NoverP * proportion * bitsPerElementLayers[i];
    }
  }
  if(print) {
    cout << "pos size: " << posSize << endl;
    cout << "neg size: " << negSize << endl;
  }
  double totalSize = posSize + negSize;
  return totalSize;
}

double calculateEFPR(std::vector<double> bitsPerElementLayers, double psi) {
    double proportion = 1.0;
    double FPRcurr = 0.0;
    double psiTrack = psi;
    for (int i = 0; i < bitsPerElementLayers.size()/2; i++) {
        double alpha1 = min(1.0, pow(2, -1 * log(2) * bitsPerElementLayers[i]));
        double alpha2 = min(1.0, pow(2, -1 * log(2) * bitsPerElementLayers[i+1]));
        proportion = proportion * alpha1 * (psi + (1 - psi) * alpha2);
        FPRcurr = FPRcurr + (1 - psiTrack) * alpha1 * (1 - alpha2);
        psiTrack = psiTrack / (psiTrack + ((1 - psiTrack) * alpha2));
    }
    if (bitsPerElementLayers.size() > 0) {
        FPRcurr += proportion * min(1.0, pow(2, -1 * log(2) * bitsPerElementLayers[bitsPerElementLayers.size()-1]));
    } else {
        FPRcurr += proportion;
    }
    return FPRcurr;
}

std::vector<double> calculateLayerFPRs(int num_layers, double constraint, double psi, double NoverP, double timeInSeconds) {
    double one_level_fpr = exp(-constraint * log(2) * log(2));
    OptimizationObjectNLOpt optObj = OptimizationObjectNLOpt(constraint, psi, NoverP);
    std::vector<double> layer_fprs;
    std::vector<double> one_layer_fprs =
            std::vector<double>(num_layers, one_level_fpr);
    for (int i = 1; i < num_layers; i++) one_layer_fprs[i] = 1;
    if (num_layers == 1) {
        layer_fprs = one_layer_fprs;
        return layer_fprs;
    }
    double *zeros = (double *) calloc(num_layers, sizeof(double));
    for (int i = 0; i < num_layers; i++) zeros[i] = 0.00000000000000000001;
    double *ones = (double *) calloc((num_layers), sizeof(double));
    for (int i = 0; i < num_layers; i++) ones[i] = 1;
    nlopt_opt equal_fpr_opt = nlopt_create(NLOPT_GN_ISRES, 1);
    nlopt_set_lower_bounds(equal_fpr_opt, zeros);
    nlopt_set_upper_bounds(equal_fpr_opt, ones);
    nlopt_set_maxtime(equal_fpr_opt, timeInSeconds);
    nlopt_add_inequality_constraint(
            equal_fpr_opt, SizeFunctionEqual,
            &optObj, constraint * .0005);
    nlopt_set_min_objective(equal_fpr_opt,
                            FprFunctionEqual,
                            &optObj);
    double equal_score = 0;
    double every_layer_fpr = .5;
    if (nlopt_optimize(equal_fpr_opt, &every_layer_fpr, &equal_score) < 0)
        printf("Equal Opt Error!!\n");
    std::vector<double> equal_fprs = std::vector<double>(num_layers, every_layer_fpr);
    unsigned int num_positive_layers = (num_layers + 1) / 2;
    double equal_fpr = pow(every_layer_fpr, num_positive_layers);
    //if (equal_layer_fprs) {
    //    layer_fprs = equal_fprs;
    //    return layer_fprs;
    //}
    if (equal_fpr > one_level_fpr) {
        printf("Using One Level Start\n");
        layer_fprs = one_layer_fprs;
    } else {
        printf("Using Equal Level Start\n");
        layer_fprs = equal_fprs;
    }
    nlopt_opt local_fpr_opt = nlopt_create(NLOPT_LN_COBYLA, num_layers);
    nlopt_set_lower_bounds(local_fpr_opt, zeros);
    nlopt_set_upper_bounds(local_fpr_opt, ones);
    nlopt_set_maxtime(
            local_fpr_opt,
            timeInSeconds);  
    nlopt_set_ftol_rel(local_fpr_opt, .000001);
    nlopt_add_inequality_constraint(
            local_fpr_opt, SizeFunctionVaried,
            &optObj, constraint * .0005);
    nlopt_set_min_objective(local_fpr_opt,
                            FprFunctionVaried,
                            &optObj);
    double variable_fpr_fpr = 1;
    nlopt_result local_ret_status =
            nlopt_optimize(local_fpr_opt, layer_fprs.data(), &variable_fpr_fpr);
    if (local_ret_status == -4)
        printf("ERROR!!!  Roundoff Errors Reached in Local Optimization\n");
    else if (local_ret_status < 0)
        printf("ERROR!!! General Error in Local Optimization\n");
    return layer_fprs;
}

int main(int argc, char *argv[]) {
  // argument 1: minimization of fpr or space
  // argument 2: constraint (either FPR or space in bits per element)
  // argument 3: psi
  // argument 4: N / P
  // Example: ./filter_optimizer fpr 10 0.8 2 0.001
  // translation: minimize fpr for a stacked filter using less than 10 bits per element, given psi=0.8, N/P = 2

  if (argc < 6) {
    cout << "There should be the following 4 arguments: the minimization (fpr or space),"
    "the constraint (the other of fpr or space), psi, N/P, timeInSeconds " << endl;
    cout << "Ex: ./filter_optimizer fpr 10 0.8 2 0.5" << endl;
    return 0;
  }

  std::string minimization = argv[1];
  double constraint = std::stod(argv[2]);
  double psi = std::stod(argv[3]);
  double NoverP = std::stod(argv[4]);
  double timeInSeconds = std::stod(argv[5]);
  cout << printf("%s, %f, %f, %f %f", minimization.c_str(), constraint, psi, NoverP, timeInSeconds) << endl;

  // ignore size minimization for given FPR for now. Focus on size -> minimum FPR

  // 1) calculate single layer FPR. Use this to get epsilon slack for best FPR allowed. 
  double bestFPR = fprForBitsPerEleBF(constraint);
  cout << "one layer FPR is: " << bestFPR << endl;

  /*OptimizationObject startObject(constraint,psi, NoverP, epsilonSlack);
  OptimizationObject bestbyFPR = optimizeFPRunderSizeConstraintDepthFirst(startObject);

  cout << "Best by Depth First " << endl;
  for (auto i: bestbyFPR.fingerprints) {
    std::cout << i << ' ';
  }
  cout << endl;
  cout << "FPR: " << bestbyFPR.FPRcurr << endl; */

  auto start = high_resolution_clock::now(); 
  std::vector<double> returnSetup = calculateLayerFPRs(5, constraint, psi, NoverP, timeInSeconds);
  std::vector<double> sizes;
  for(unsigned int i = 0; i < returnSetup.size(); i++) {
    sizes.push_back(sizeForFprBF(returnSetup[i]));
  }
  auto stop = high_resolution_clock::now(); 
  auto duration = duration_cast<milliseconds>(stop - start);
  cout << "duration is: " << duration.count() << " milliseconds" << endl;

  for (auto i: sizes)
    std::cout << i << ' ';
  cout << endl;

  cout << "size of best setup: " << calculateSize(sizes, NoverP, false) << endl;

  cout << "FPR of best setup: " << calculateEFPR(sizes, psi) << endl;

  return 0;
}
