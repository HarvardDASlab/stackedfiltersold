import numpy as np

def bf_size(alpha_layer, Tl, NoverP):
    sum_one = 0
    for i in range(int((Tl - 1) / 2) + 1):
        sum_one += np.power(alpha_layer,i)
    sum_two = 0
    for j in range(1,int((Tl - 1) / 2) + 1):
        sum_two += np.power(alpha_layer,j)
    result = -1 * np.log(alpha_layer) * (sum_one + sum_two)
    print("Tl: {}, alpha_layer: {}, result: {}".format(Tl, alpha_layer, result))
    return result

def get_alpha_val(Tl, desiredAlpha):
    return np.power(desiredAlpha, 1.0 / ((Tl+1) / 2))

alpha = 0.01
NoverP = 1
minSize = -1 * np.log(alpha)
minTl = 1
for i in range(3,1001,2):
    Tl = i
    alpha_layer = get_alpha_val(Tl, alpha)
    total_size = bf_size(alpha_layer, Tl, NoverP)
    if (total_size < minSize):
        minSize = total_size
        minTl = Tl
print("Tl: {}, size: {}".format(minTl, minSize))

