import numpy as np
import sys

def getFalsePositiveRate(n, En, m):
    k = np.log(2) * m / En
    term1 = 1 - np.exp(-1 * np.log(2) * n / En)
    falsePositiveRate = np.power(1 - term1, k)
    return falsePositiveRate


def calculateExpectedEFPR(psi, fprEachLayer, numLayers):
    known_fpr = np.power(fprEachLayer, (numLayers + 1 / 2))
    unknown_fpr = 0
    for i in range(1, (numLayers+1) // 2):
        temp_fpr = np.power(fprEachLayer, (2 * i - 1))
        unknown_fpr += temp_fpr * (1 - fprEachLayer)
    unknown_fpr += np.power(fprEachLayer, numLayers)
    return (psi * known_fpr) + ((1 - psi) * unknown_fpr)

def calculateActualEFPR(psi, observedFPRs, survivingNegatives, numNegatives, numLayers):
    known_fprs_observed = np.squeeze(psi * (1.0 * survivingNegatives) / numNegatives)
    unknown_fprs = np.full((numTrials), 0.0)
    rolling_fprs = np.full((numTrials), 1.0)
    for i in range(1, (numLayers+1) // 2):
        rolling_fprs = np.multiply(rolling_fprs, observedFPRs[:,(2 * i - 1)])
        unknown_fprs += np.multiply(rolling_fprs, (1 - observedFPRs[:,2 * i]))
        rolling_fprs = np.multiply(rolling_fprs, observedFPRs[:,(2 * i)])
    unknown_fprs += np.multiply(rolling_fprs, observedFPRs[:,-1])
    total_fprs = (psi * known_fprs_observed) + (unknown_fprs * (1 - psi))
    return total_fprs

def simulatedVsExpectedFPR(psi, fprEachLayer, numLayers, numPositives, numNegatives, numTrials):
    fprLayer1 = fprEachLayer
    negLogFPR = -1 * np.log(fprEachLayer)
    survivingNegatives = np.full((numTrials,1), numNegatives)
    survivingPositives = np.full((numTrials,1), numPositives)
    observedFPRs = np.full((numTrials,1), fprEachLayer)
    expectedFPR = calculateExpectedEFPR(psi, fprEachLayer, numLayers)
    for i in range(numLayers):
        if (i % 2 == 0):
            expectedPositives = np.power(fprEachLayer, i/2) * numPositives
            m = expectedPositives * (negLogFPR / (np.power(np.log(2), 2)))
            actualFPRs = getFalsePositiveRate(survivingPositives,expectedPositives, m)
            if (i != 0):
                observedFPRs = np.concatenate((observedFPRs, actualFPRs),axis=1)
            survivingNegatives = np.random.binomial(survivingNegatives, actualFPRs, (numTrials,1))
        else:
            expectedNegatives = np.power(fprEachLayer, (i+1)/2) * numNegatives
            m = expectedNegatives * (negLogFPR / (np.power(np.log(2), 2)))
            actualFPRs = getFalsePositiveRate(survivingNegatives,expectedNegatives, m)
            observedFPRs = np.concatenate((observedFPRs, actualFPRs),axis=1)
            survivingPositives = np.random.binomial(survivingPositives, actualFPRs, (numTrials,1))
    ## get results
    totalObservedFPRs = np.squeeze(calculateActualEFPR(psi, observedFPRs, survivingNegatives, numNegatives, numLayers))
    print("FPR OF EACH LAYER IN THE STACK, 95TH PERCENTILE")
    print(np.percentile(observedFPRs, 95, axis=0))
    print("TOTAL FPR NUMBERS (OF THE FILTER)")
    print("expected FPR: {}, observedFPR mean: {}, totalFPR 95th percentile: {}".format(expectedFPR, np.mean(totalObservedFPRs), np.percentile(totalObservedFPRs, 95)))

numTrials = int(sys.argv[1])
numPositives = int(sys.argv[2])
numNegatives = int(sys.argv[3])
fprEachLayer = float(sys.argv[4])
numLayers = int(sys.argv[5])
psi = float(sys.argv[6])

"""numTrials = 10000
numPositives = 5000
numNegatives = 5000
fprEachLayer = 0.07
numLayers = 9
psi = 0.5 """
simulatedVsExpectedFPR(psi, fprEachLayer, numLayers, numPositives, numNegatives, numTrials)

